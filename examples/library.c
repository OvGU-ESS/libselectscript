#include <stdlib.h>
#include <stdio.h>
#include <selectscript.h>
#include <library.h>

int *add(int *a, int *b)
{
    int *result = calloc(1, sizeof(int));

    *result = *a + *b;

    return result;
}

int main(void)
{
    add_library_function(FID_ADD, add);

    int a = 1;
    int b = 2;

    int *c = library.add(&a, &b);

    printf("%d + %d = %d\n", a, b, *c);

    free(c);

    return EXIT_SUCCESS;
}
