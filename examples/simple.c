#include <stdlib.h>
#include <selectscript.h>
#include <stop_watch.h>
#include <astdump.h>
#include <deque.h>
#include <result.h>
#include <hashmap.h>

int main(void)
{
    clock_t all = start_watch();

    /* sample script */
    const char *script =
        "colors = ['red', 'green', 'blue', 'yellow'];\n"

        "select\n"
        "a.this, b.this, c.this, d.this,\n"
        "e.this, f.this, g.this, h.this\n"

        "from\n"
        "a=colors, b=colors, c=colors, d=colors,\n"
        "e=colors, f=colors, g=colors, h=colors\n"

        "where\n"
        "a.this != b.this and a.this != c.this and\n"
        "a.this != d.this and a.this != e.this and\n"
        "a.this != f.this and\n"
        "b.this != c.this and b.this != d.this and\n"
        "b.this != f.this and b.this != g.this and\n"
        "c.this != d.this and c.this != g.this and\n"
        "c.this != h.this and\n"
        "d.this != e.this and d.this != h.this and\n"
        "e.this != f.this and e.this != h.this and\n"
        "f.this != g.this and g.this != h.this as dict;";

    /* parse the script */
    clock_t watch = start_watch();
    struct ss_state *state = ss_new(script);
    stop_watch(watch, "ss_new");

    /* output AST for debugging */
    watch = start_watch();
    dump_ast(state->parser_state.ast, "ast.dump");
    stop_watch(watch, "dump_ast");

    /* evaluate script */
    watch = start_watch();
    ss_evaluate(state);
    stop_watch(watch, "ss_evaluate");

    /* print result */
    if (state->result) {
        struct deque_iterator *di_global = deque_iterator_init(&(state->result));
        struct result *local = NULL;
        while ((local = deque_iterator_next(di_global)) != NULL) {
            struct deque_iterator *di_local = deque_iterator_init(&(local->data));

            if (local->type == R_LIST) {
                struct container *result = NULL;

                fprintf(stderr, "[");

                if ((result = deque_iterator_next(di_local)) != NULL) {
                    fprintf(stderr, "\"%s\"", result->strng);
                }

                while ((result = deque_iterator_next(di_local)) != NULL) {
                    fprintf(stderr, ", \"%s\"", result->strng);
                }

                fprintf(stderr, "]\n");
            } else {
                struct hashmap_entry *entry = NULL;

                fprintf(stderr, "{");

                struct container *c = NULL;
                if ((entry = deque_iterator_next(di_local)) != NULL) {
                    c = entry->value;
                    fprintf(stderr, "\"%s\": \"%s\"", entry->key, c->strng);
                }

                while ((entry = deque_iterator_next(di_local)) != NULL) {
                    c = entry->value;
                    fprintf(stderr, ", \"%s\": \"%s\"", entry->key, c->strng);
                }

                fprintf(stderr, "}\n");
            }
        }
    }

    /* free SelectScript interpreter */
    ss_free(state);

    stop_watch(all, "main");

    return EXIT_SUCCESS;
}

// int main(int argc, char **argv)
// {
//     to("dd"+a.this, f(a.this))
//
//     struct ss_state *state = ss_new("select func(this) from 'string' where a == 1 start with phrase connect by no cycle unique memorize 23 maximum 42 phrase stop with phrase group by phrase having phrase order by phrase asc as list;");
//
//     state->library->lib_add = lib_add;
//
//     int a = 1, b = 2;
//     printf("lib_add(1, 2) = %d\n", *(int *)(state->library->lib_add(&a, &b)));
//
//     ss_env_add(state, ENV_VOID_FUNCTION, "add", add);
//
//     struct environment *environment = hashmap_get(state->environment, "add");
//     ((void (*)())(environment->void_function))(1, 2);
//
//     int *result = state->result;
//     printf("%s => %d\n", script, *result);
//
// }
