#include <stdlib.h>
#include <stdio.h>
#include <selectscript.h>
#include <result.h>
#include <env.h>
#include <stop_watch.h>

int main(void)
{
    clock_t all = start_watch();

    const char *script =
        "select\n"
        "a.this, b.this, c.this, d.this,\n"
        "e.this, f.this, g.this, h.this\n"

        "from\n"
        "a=colors, b=colors, c=colors, d=colors,\n"
        "e=colors, f=colors, g=colors, h=colors\n"

        "where\n"
        "a.this != b.this and a.this != c.this and\n"
        "a.this != d.this and a.this != e.this and\n"
        "a.this != f.this and\n"
        "b.this != c.this and b.this != d.this and\n"
        "b.this != f.this and b.this != g.this and\n"
        "c.this != d.this and c.this != g.this and\n"
        "c.this != h.this and\n"
        "d.this != e.this and d.this != h.this and\n"
        "e.this != f.this and e.this != h.this and\n"
        "f.this != g.this and g.this != h.this as dict;";

    const char *colors[] = {
        "red",
        "green",
        "blue",
        "yellow",
        NULL
    };

    struct deque *list = NULL;

    for (int i = 0; colors[i] != NULL; i++) {
        struct container *container = malloc(sizeof(struct container));
        container->type = C_STRING;
        container->strng = strdup(colors[i]);

        deque_push_last(&list, container);
    }

    add_environment_element(ENV_LIST, "colors", list);

    clock_t watch = start_watch();
    struct ss_state *state = ss_new(script);
    stop_watch(watch, "ss_new");

    watch = start_watch();
    ss_evaluate(state);
    stop_watch(watch, "ss_evaluate");

    /* print result */
    if (state->result) {
        struct deque_iterator *di_global = deque_iterator_init(&(state->result));
        struct result *local = NULL;
        while ((local = deque_iterator_next(di_global)) != NULL) {
            struct deque_iterator *di_local = deque_iterator_init(&(local->data));

            if (local->type == R_LIST) {
                struct container *result = NULL;

                fprintf(stderr, "[");

                if ((result = deque_iterator_next(di_local)) != NULL) {
                    fprintf(stderr, "\"%s\"", result->strng);
                }

                while ((result = deque_iterator_next(di_local)) != NULL) {
                    fprintf(stderr, ", \"%s\"", result->strng);
                }

                fprintf(stderr, "]\n");
            } else {
                struct hashmap_entry *entry = NULL;

                fprintf(stderr, "{");

                struct container *c = NULL;
                if ((entry = deque_iterator_next(di_local)) != NULL) {
                    c = entry->value;
                    fprintf(stderr, "\"%s\": \"%s\"", entry->key, c->strng);
                }

                while ((entry = deque_iterator_next(di_local)) != NULL) {
                    c = entry->value;
                    fprintf(stderr, ", \"%s\": \"%s\"", entry->key, c->strng);
                }

                fprintf(stderr, "}\n");
            }
        }
    }

    /* free SelectScript interpreter */
    ss_free(state);

    struct deque_iterator *di = deque_iterator_init(&list);
    struct container *c = NULL;
    while ((c = deque_iterator_next(di)) != NULL) {
        //fprintf(stderr, "%s\n", c->strng);
        free(c->strng);
    }

    deque_iterator_free(di);

    deque_free(&list, free);

    stop_watch(all, "main");

    return EXIT_SUCCESS;
}
