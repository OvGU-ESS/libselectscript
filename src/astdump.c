#include <stdio.h>
#include <string.h>
#include <hashmap.h>
#include <deque.h>
#include "astdump.h"
#include "payload.h"

const char *astdump_type_names[] = {
    "N_TRANSLATION_UNIT",

    "N_SELECTION",
    "N_SELECT",
    "N_SELECTOR",
    "N_WHERE",

    "N_ASSIGNMENT",
    "N_EXPRESSION_SEQUENCE",
    "N_ASSIGN_EXPRESSION",
    "N_VALUE",
    "N_LIST",
    "N_VARIABLE",
    "N_FUNCTION_CALL",
    "N_IF",

    "N_LOGIC_OR",
    "N_LOGIC_XOR",
    "N_LOGIC_AND",
    "N_LOGIC_NOT",

    "N_COMPARISON_IN",
    "N_COMPARISON_EQ",
    "N_COMPARISON_NE",
    "N_COMPARISON_GE",
    "N_COMPARISON_GT",
    "N_COMPARISON_LE",
    "N_COMPARISON_LT",

    "N_ARITHMETIC_ADD_SUB",
    "N_ARITHMETIC_MUL_DIV_MOD",
    "N_ARITHMETIC_POW",
    "N_ARITHMETIC_NEGATION"
};

int ptr_to_str(char **str, void *ptr)
{
    return asprintf(str, "#%p ", ptr);
}

void dump_ast(struct node *ast, const char *path)
{
    FILE *fp = fopen(path, "w");

    struct tree_iterator *it = tree_iterator_init(&ast, PREORDER);
    struct node *current = NULL;
    struct stack *parents = NULL;

    struct deque *transitions = NULL;

    while ((current = tree_iterator_next(it)) != NULL) {
        while (current->parent != NULL && current->parent != stack_peek(parents)) {
            fwrite(")", 1, 1, fp);
            stack_pop(&parents);
        }

        fwrite("(", 1, 1, fp);
        struct payload *payload = current->payload;
        const char *name = astdump_type_names[payload->type];

        char *id = NULL;
        int id_len = ptr_to_str(&id, current);
        fwrite(id, id_len, 1, fp);
        fwrite(name, strlen(name), 1, fp);
        free(id);

        switch (payload->type) {
            case N_SELECTION:
                fprintf(fp, ": flags [");
                payload->selection.flags.where ? fprintf(fp, "where") : fprintf(fp, "-");
                fprintf(fp, "]");
                break;
            case N_VALUE:
                switch (payload->value.type) {
                    case V_STRING:
                        fprintf(fp, ": %s", payload->value.str);
                        break;
                    case V_FLOAT:
                        fprintf(fp, ": %f", payload->value.flt);
                        break;
                    case V_INTEGER:
                        fprintf(fp, ": %d", payload->value.ntgr);
                        break;
                    case V_BOOLEAN:
                        if (payload->value.bln) {
                            fprintf(fp, ": %s", "true");
                        } else {
                            fprintf(fp, ": %s", "false");
                        }
                        break;
                    default:
                        ;
                }
                break;
            case N_VARIABLE:
                if (payload->variable.phrase) {
                    fprintf(fp, ": %s", payload->variable.phrase);
                } else {
                    fprintf(fp, ": NULL");
                }

                if (payload->variable.ref) {
                    void **node_ptr = malloc(2 * sizeof(void *));
                    node_ptr[0] = current;
                    node_ptr[1] = payload->variable.ref;

                    deque_push_last(&transitions, node_ptr);
                }
                break;
            case N_ASSIGNMENT:
                fprintf(fp, ": %s", payload->assignment.phrase);
                break;
            case N_FUNCTION_CALL:
                fprintf(fp, ": %s", payload->function_call.phrase);
                break;
            case N_SELECTOR:
                fprintf(fp, ": %s", payload->selector.phrase);
                break;
            case N_ASSIGN_EXPRESSION:
                fprintf(fp, ": %s", payload->assign_expression.phrase);
                break;
            default:
                ;
        }

        stack_push(&parents, current);
    }

    while (stack_pop(&parents) != NULL) {
        fwrite(")", 1, 1, fp);
    }

    fwrite("\n", 1, 1, fp);

    void **elem = NULL;
    while ((elem = deque_pop_first(&transitions)) != NULL) {
        char *from, *to;
        ptr_to_str(&from, elem[0]);
        ptr_to_str(&to, elem[1]);

        fprintf(fp, "%s to %s;\n", from, to);

        free(from);
        free(to);
    }

    tree_iterator_free(it);
    fclose(fp);
}
