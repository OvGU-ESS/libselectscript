#ifndef PARSER_STATE_H
#define PARSER_STATE_H

#include <tree.h>

enum states {
    OK,
    ERROR
};

struct parser_state {
    enum states state;
    struct node *ast;
};

#endif
