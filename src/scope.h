#ifndef SCOPE_H
#define SCOPE_H

#include "payload.h"

void link_references(struct node *node);

#endif
