#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include "selectscript.h"
#include "astdump.h"
#include "parser_state.h"
#include "lexer.h"
#include "parser.h"
#include "parser_signatures.h"
#include "interpreter.h"
#include "scope.h"
#include "stop_watch.h"
#include "container.h"

struct library library = {0};
struct hashmap *environment = NULL;

void add_library_function(enum function_id fid, ...)
{
    va_list ap;
    va_start(ap, fid);

    switch (fid) {
        case FID_ADD:
            library.add = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_SUB:
            library.sub = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_MUL:
            library.mul = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_DIV:
            library.div = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_MOD:
            library.mod = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_POW:
            library.pow = va_arg(ap, void *(*)(void *, void *));
            break;
        case FID_EQ:
            library.eq = va_arg(ap, int (*)(void *, void *));
            break;
        case FID_NE:
            library.ne = va_arg(ap, int (*)(void *, void *));
            break;
        case FID_GE:
            library.ge = va_arg(ap, int (*)(void *, void *));
            break;
        case FID_GT:
            library.gt = va_arg(ap, int (*)(void *, void *));
            break;
        case FID_LE:
            library.le = va_arg(ap, int (*)(void *, void *));
            break;
        case FID_LT:
            library.lt = va_arg(ap, int (*)(void *, void *));
            break;
    }

    va_end(ap);
}

void add_environment_element(enum env_type type, const char *name,
                             void *element)
{
    struct env *env = calloc(1, sizeof(struct env));
    env->type = type;

    if (type == ENV_VALUE) {
        env->value = element;
    } else if (type == ENV_LIST) {
        env->list = element;
    } else {
        return;
    }

    hashmap_put(&environment, name, env);
}

struct ss_state *ss_new(const char *script)
{
    struct ss_state *state = malloc(sizeof(struct ss_state));

    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string(script, scanner);

    void *parser = ParseAlloc(malloc);

    int lex_code = 0;
    struct token *token = NULL;
    while ((lex_code = yylex(scanner))) {
        if (lex_code == -1) {
            break;
        }

        token = malloc(sizeof(struct token));
        token->content = strdup(yyget_text(scanner));
        token->lineno = yyget_lineno(scanner);
        Parse(parser, lex_code, token, &(state->parser_state));
    }

    Parse(parser, 0, NULL, &(state->parser_state));
    ParseFree(parser, free);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);

    /* merge in environment */
    struct payload *tu_payload = state->parser_state.ast->payload;
    hashmap_update(&(tu_payload->translation_unit.scope), environment);

    /* link references */
    link_references(state->parser_state.ast);

    return state;
}

void ss_evaluate(struct ss_state *state)
{
    state->result = interpret(&(state->parser_state));
}

void ss_free(struct ss_state *state)
{
    if (state) {
        free(state);
    }
}
