#include <tree.h>
#include <hashmap.h>
#include <stdlib.h>
#include "scope.h"
#include "env.h"

struct node *find_scope_for(struct node *node)
{
    struct node *temp = node;

    while ((temp = temp->parent) != NULL) {
        struct payload *payload = temp->payload;
        if (payload->type == N_TRANSLATION_UNIT ||
                payload->type == N_WHERE) {
            return temp;
        }
    }

    return NULL;
}

struct env *resolve_reference(struct node *node, const char *id)
{
    struct node *scope = node;
    struct env *ref = NULL;

    while ((scope = find_scope_for(scope)) != NULL && ref == NULL) {
        struct payload *temp = scope->payload;
        switch (temp->type) {
            case N_TRANSLATION_UNIT:
                ref = hashmap_get(temp->translation_unit.scope, id);
                break;
            case N_WHERE:
                ref = hashmap_get(temp->where.scope, id);
                break;
            default:
                ref = NULL;
        }
    }

    return ref;
}

void link_references(struct node *node)
{
    struct tree_iterator *it = tree_iterator_init(&node, POSTORDER);

    struct node *temp = NULL;
    while ((temp = tree_iterator_next(it)) != NULL) {
        char *id = NULL;
        struct payload *payload = temp->payload;

        switch (payload->type) {
            case N_VARIABLE:
                id = payload->variable.phrase;
                payload->variable.ref = resolve_reference(temp, id);
                break;
            default:
                ;
        }
    }

    tree_iterator_free(it);
}
