#include <time.h>
#include <stdio.h>
#include "stop_watch.h"

clock_t start_watch()
{
    return clock();
}

void stop_watch(clock_t watch, const char *name)
{
    fprintf(stderr, "%s took %f seconds.\n", name, (double)(clock() - watch) / CLOCKS_PER_SEC);
}
