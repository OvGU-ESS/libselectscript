#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "payload.h"

struct payload *payload_create(enum type type, ...)
{
    va_list ap;
    va_start(ap, type);

    struct payload *payload = malloc(sizeof(struct payload));
    payload->type = type;

    switch (type) {
        case N_TRANSLATION_UNIT:
            payload->translation_unit.scope = NULL;
            break;
        case N_SELECTION:
            payload->selection.flags.where = 0;
            payload->selection.flags.as = 0;
            payload->selection.scope = NULL;
            payload->selection.result_type = R_NONE;
            break;
        case N_VALUE:
            switch (va_arg(ap, enum value_type)) {
                case V_STRING:
                    payload->value.type = V_STRING;
                    payload->value.str = strdup((char *)va_arg(ap, char *));
                    break;
                case V_FLOAT:
                    payload->value.type = V_FLOAT;
                    payload->value.flt = atof(va_arg(ap, char *));
                    break;
                case V_INTEGER:
                    payload->value.type = V_INTEGER;
                    payload->value.ntgr = atoi(va_arg(ap, char *));
                    break;
                case V_BOOLEAN:
                    payload->value.type = V_BOOLEAN;
                    payload->value.bln =
                        strcmp(va_arg(ap, char *), "true") == 0 ? 1 : 0;
                    break;
                default:
                    ;
            }
            break;
        case N_VARIABLE:
            ;
            char *phrase = va_arg(ap, char *);
            if (phrase) {
                payload->variable.phrase = strdup(phrase);
            } else {
                payload->variable.phrase = NULL;
            }
            payload->variable.ref = NULL;
            break;
        case N_ASSIGNMENT:
            payload->assignment.phrase = strdup((char *)va_arg(ap, char *));
            break;
        case N_FUNCTION_CALL:
            payload->function_call.phrase = strdup((char *)va_arg(ap, char *));
            break;
        case N_SELECTOR:
            payload->selector.phrase = strdup((char *)va_arg(ap, char *));
            break;
        case N_ASSIGN_EXPRESSION:
            payload->assign_expression.phrase = strdup((char *)va_arg(ap, char *));
            break;
        case N_WHERE:
            payload->where.scope = NULL;
            break;
        case N_ARITHMETIC_ADD_SUB:
            payload->arithmetic_add_sub.type = va_arg(ap, enum arithmetic_add_sub_type);
            break;
        case N_ARITHMETIC_MUL_DIV_MOD:
            payload->arithmetic_mul_div_mod.type = va_arg(ap, enum arithmetic_mul_div_mod_type);
            break;
        default:
            ;
    }

    va_end(ap);

    return payload;
}

void payload_free(void *payload)
{
    free(payload);
}
