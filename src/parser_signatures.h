#ifndef PARSER_SIGNATURES_H
#define PARSER_SIGNATURES_H

#include <stdlib.h>
#include "parser_state.h"
#include "token.h"

void *ParseAlloc(void *(*allocProc)(size_t));
void Parse(void *, int, const struct token *, struct parser_state *);
void ParseFree(void *, void (*freeProc)(void *));

#endif
