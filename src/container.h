#ifndef CONTAINER_H
#define CONTAINER_H

#include <tree.h>

enum container_type {
    C_STRING,
    C_FLOAT,
    C_INTEGER,
    C_BOOLEAN,
    C_EXTERNAL
};

struct container {
    enum container_type type;
    union {
        char *strng;
        float flt;
        int ntgr;
        int bln;
        struct node *node;
        void *ext;
    };
};

#endif
