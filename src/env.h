#ifndef ENV_H
#define ENV_H

#include <deque.h>
#include <tree.h>
#include "container.h"

enum env_type {
    ENV_NODE,
    ENV_VALUE,
    ENV_LIST
};

struct env {
    enum env_type type;
    union {
        struct node *node;
        struct container *value;
        struct deque *list;
    };
};

#endif
