#ifndef STOP_WATCH_H
#define STOP_WATCH_H

#include <time.h>

clock_t start_watch();

void stop_watch(clock_t watch, const char *name);

#endif
