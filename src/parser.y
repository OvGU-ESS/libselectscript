%include
{
    #include <stdlib.h>
    #include <stdio.h>
    #include <assert.h>
    #include <limits.h>
    #include <tree.h>
    #include <deque.h>
    #include <hashmap.h>
    #include "parser_state.h"
    #include "token.h"
    #include "payload.h"
    #include "container.h"
    #include "result.h"
    #include "env.h"

    struct stack *global_scope = NULL;

    char *str_clean(const char *str)
    {
        char *ret = NULL;

        if (strlen(str) > 0) {
            if (str[0] == '"' || str[0] == '\'') {
                ret = malloc((strlen(str) - 2 + 1) * sizeof(char));
                strncpy(ret, str + 1, strlen(str) - 2);
            }
        }

        return ret;
    };
}

%token_type { const struct token * }

%token_destructor
{
    free(((struct token *)$$)->content);
    free((struct token *)$$);
    (void)parser_state->state;
}

%default_type { struct node * }
%type selector_sequence { struct deque * }
%type statement_sequence { struct deque * }
%type as { enum result_type }

%extra_argument { struct parser_state *parser_state }

%syntax_error
{
    if (TOKEN) {
        fprintf(stderr, "Error parsing input on line %d!\n", TOKEN->lineno);
    } else {
        fprintf(stderr, "Error parsing input!\n");
    }

    parser_state->state = ERROR;
}

/* TRANSLATION_UNIT */

translation_unit ::= statement_sequence(SS).
{
    parser_state->ast = tree_create_node(
        payload_create(N_TRANSLATION_UNIT),
        1, deque_pop_first(&SS)
    );

    for (struct node *temp = deque_pop_first(&SS); temp != NULL; temp = deque_pop_first(&SS)) {
        parser_state->ast = tree_append_node(parser_state->ast, temp);
    }

    struct payload *payload = parser_state->ast->payload;

    struct hashmap_entry *temp;
    while ((temp = stack_pop(&global_scope)) != NULL) {
        struct env *env = malloc(sizeof(struct env));
        env->type = ENV_NODE;
        env->node = temp->value;

        hashmap_put(&(payload->translation_unit.scope), temp->key, env);

        free(temp->key);
        free(temp);
    }

    parser_state->state = OK;
}
translation_unit ::= error.

/* STATEMENT_SEQUENCE */

statement_sequence(NODE) ::= statement_sequence(SS) statement(S).
{
    deque_push_last(&SS, S);
    NODE = SS;
}
statement_sequence(NODE) ::= statement(S).
{
    struct deque *deque = NULL;
    deque_push_last(&deque, S);
    NODE = deque;
}

/* STATEMENT */

statement(NODE) ::= selection(S) SEMIC. { NODE = S; }
statement(NODE) ::= assignment(A) SEMIC. { NODE = A; }

/* SELECTION */

selection(NODE) ::= select(S) from(F) where(W) as(A).
{
    struct payload *payload = payload_create(N_SELECTION);

    NODE = tree_create_node(
        payload,
        2, S, F
    );

    if (W) {
        payload->selection.flags.where = 1;
        NODE = tree_append_node(NODE, W);
    }

    if (A != R_NONE) {
        payload->selection.flags.as = 1;
        payload->selection.result_type = A;
    }
}

select(NODE) ::= SELECT MUL.
{
    NODE = tree_create_node(
        payload_create(N_SELECT),
        0
    );
}
select(NODE) ::= SELECT selector_sequence(SS).
{
    NODE = tree_create_node(
        payload_create(N_SELECT),
        1, deque_pop_first(&SS)
    );

    for (struct node *temp = deque_pop_first(&SS); temp != NULL; temp = deque_pop_first(&SS)) {
        NODE = tree_append_node(NODE, temp);
    }
}

selector_sequence(NODE) ::= selector_sequence(SS) COMMA selector(S).
{
    deque_push_last(&SS, S);
    NODE = SS;
}
selector_sequence(NODE) ::= selector(S).
{
    struct deque *deque = NULL;
    deque_push_last(&deque, S);
    NODE = deque;
}

selector(NODE) ::= PHRASE(P).
{
    NODE = tree_create_node(
        payload_create(N_SELECTOR, P->content),
        0
    );
}
selector(NODE) ::= function_call(FC). { NODE = FC; }
selector(NODE) ::= this(T). { NODE = T; }

from(NODE) ::= FROM expression_sequence(ES). { NODE = ES; }

where(NODE) ::= WHERE logic_expression(LE).
{
    NODE = tree_create_node(
        payload_create(N_WHERE),
        1, LE
    );
}
where(NODE) ::= . { NODE = NULL; }

as(NODE) ::= AS LIST. { NODE = R_LIST; }
as(NODE) ::= AS DICT. { NODE = R_DICT; }
as(NODE) ::= . { NODE = R_NONE; }

/* ASSIGNMENT */

assignment(NODE) ::= PHRASE(P) LBRACE expression(E1) RBRACE ASSIGN expression(E2).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGNMENT, P->content),
        2, E1, E2
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
    stack_push(&global_scope, temp);
}
assignment(NODE) ::= PHRASE(P) LBRACE RBRACE ASSIGN expression(E).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGNMENT, P->content),
        1, E
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
    stack_push(&global_scope, temp);
}
assignment(NODE) ::= PHRASE(P) ASSIGN expression(E).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGNMENT, P->content),
        1, E
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
    stack_push(&global_scope, temp);
}

/* EXPRESSION */

expression_sequence(NODE) ::= expression_sequence(ES) COMMA expression(E).
{
    NODE = ES;
    NODE = tree_append_node(NODE, E);
}
expression_sequence(NODE) ::= expression(E).
{
    NODE = tree_create_node(
        payload_create(N_EXPRESSION_SEQUENCE),
        1, E
    );
}

expression(NODE) ::= assign_expression(AE). { NODE = AE; }
expression(NODE) ::= logic_expression(LE). { NODE = LE; }
expression(NODE) ::= arithmetic_expression(AE). { NODE = AE; }

/* ASSIGN_EXPRESSION */
assign_expression(NODE) ::= PHRASE(P) ASSIGN expression(E).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGN_EXPRESSION, P->content),
        1, E
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
}
assign_expression(NODE) ::= PHRASE(P) LBRACE RBRACE ASSIGN expression(E).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGN_EXPRESSION, P->content),
        1, E
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
}
assign_expression(NODE) ::= PHRASE(P) LBRACE expression(LE) RBRACE ASSIGN expression(RE).
{
    NODE = tree_create_node(
        payload_create(N_ASSIGN_EXPRESSION, P->content),
        2, LE, RE
    );

    struct hashmap_entry *temp = malloc(sizeof(struct hashmap_entry));
    temp->key = strdup(P->content);
    temp->value = NODE;
}

/* LOGIC_EXPRESSION */

logic_expression(NODE) ::= logic_or(LO). { NODE = LO; }

logic_or(NODE) ::= logic_or(LO) OR logic_xor(LX).
{
    NODE = tree_create_node(
        payload_create(N_LOGIC_OR),
        2, LO, LX
    );
}
logic_or(NODE) ::= logic_xor(LX). { NODE = LX; }

logic_xor(NODE) ::= logic_xor(LX) XOR logic_and(LA).
{
    NODE = tree_create_node(
        payload_create(N_LOGIC_XOR),
        2, LX, LA
    );
}
logic_xor(NODE) ::= logic_and(LA). { NODE = LA; }

logic_and(NODE) ::= logic_and(LA) AND logic_not(LN).
{
    NODE = tree_create_node(
        payload_create(N_LOGIC_AND),
        2, LA, LN
    );
}
logic_and(NODE) ::= logic_not(LN). { NODE = LN; }

logic_not(NODE) ::= NOT logic_not(LN).
{
    NODE = tree_create_node(
        payload_create(N_LOGIC_NOT),
        1, LN
    );
}
logic_not(NODE) ::= comparison_expression(CE). { NODE = CE; }

/* COMPARISON_EXPRESSION */

comparison_expression(NODE) ::= comparison_in(CI). { NODE = CI; }
comparison_expression(NODE) ::= comparison_eq(CE). { NODE = CE; }
comparison_expression(NODE) ::= comparison_ne(CN). { NODE = CN; }
comparison_expression(NODE) ::= comparison_ge(CG). { NODE = CG; }
comparison_expression(NODE) ::= comparison_gt(CG). { NODE = CG; }
comparison_expression(NODE) ::= comparison_le(CL). { NODE = CL; }
comparison_expression(NODE) ::= comparison_lt(CL). { NODE = CL; }

comparison_in(NODE) ::= arithmetic_expression(AE) IN atomic(A).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_IN),
        2, AE, A
    );
}

comparison_eq(NODE) ::= arithmetic_expression(AEL) EQ arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_EQ),
        2, AEL, AER
    );
}

comparison_ne(NODE) ::= arithmetic_expression(AEL) NE arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_NE),
        2, AEL, AER
    );
}

comparison_ge(NODE) ::= arithmetic_expression(AEL) GE arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_GE),
        2, AEL, AER
    );
}

comparison_gt(NODE) ::= arithmetic_expression(AEL) GT arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_GT),
        2, AEL, AER
    );
}

comparison_le(NODE) ::= arithmetic_expression(AEL) LE arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_LE),
        2, AEL, AER
    );
}

comparison_lt(NODE) ::= arithmetic_expression(AEL) LT arithmetic_expression(AER).
{
    NODE = tree_create_node(
        payload_create(N_COMPARISON_LT),
        2, AEL, AER
    );
}

/* ARITHMETIC_EXPRESSION */

arithmetic_expression(NODE) ::= arithmetic_add_sub(AAS). { NODE = AAS; }

arithmetic_add_sub(NODE) ::= arithmetic_add_sub(AAS) ADD arithmetic_mul_div_mod(AMDM).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_ADD_SUB, AAS_ADD),
        2, AAS, AMDM
    );
}
arithmetic_add_sub(NODE) ::= arithmetic_add_sub(AAS) SUB arithmetic_mul_div_mod(AMDM).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_ADD_SUB, AAS_SUB),
        2, AAS, AMDM
    );
}
arithmetic_add_sub(NODE) ::= arithmetic_mul_div_mod(AMDM). { NODE = AMDM; }

arithmetic_mul_div_mod(NODE) ::= arithmetic_mul_div_mod(AMDM) MUL arithmetic_pow(AP).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_MUL_DIV_MOD, AMDM_MUL),
        2, AMDM, AP
    );
}
arithmetic_mul_div_mod(NODE) ::= arithmetic_mul_div_mod(AMDM) DIV arithmetic_pow(AP).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_MUL_DIV_MOD, AMDM_DIV),
        2, AMDM, AP
    );
}
arithmetic_mul_div_mod(NODE) ::= arithmetic_mul_div_mod(AMDM) MOD arithmetic_pow(AP).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_MUL_DIV_MOD, AMDM_MOD),
        2, AMDM, AP
    );
}
arithmetic_mul_div_mod(NODE) ::= arithmetic_pow(AP). { NODE = AP; }

arithmetic_pow(NODE) ::= arithmetic_pow(AP) POW arithmetic_negation(AN).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_POW),
        2, AP, AN
    );
}
arithmetic_pow(NODE) ::= arithmetic_negation(AN). { NODE = AN; }

arithmetic_negation(NODE) ::= SUB arithmetic_negation(AN).
{
    NODE = tree_create_node(
        payload_create(N_ARITHMETIC_NEGATION),
        1, AN
    );
}
arithmetic_negation(NODE) ::= ADD arithmetic_negation(AN). { NODE = AN; }
arithmetic_negation(NODE) ::= primary_expression(PE). { NODE = PE; }

primary_expression(NODE) ::= LPAREN expression(E) RPAREN. { NODE = E; }
primary_expression(NODE) ::= atomic(A). { NODE = A; }

/* ATOMIC */

atomic(NODE) ::= value(V). { NODE = V; }
atomic(NODE) ::= variable(V). { NODE = V; }
atomic(NODE) ::= function_call(FC). { NODE = FC; }

/* VALUE */

value(NODE) ::= STRING(S).
{
    NODE = tree_create_node(
        payload_create(N_VALUE, V_STRING, str_clean(S->content)),
        0
    );
    free(S->content);
}
value(NODE) ::= FLOAT(F).
{
    NODE = tree_create_node(
        payload_create(N_VALUE, V_FLOAT, F->content),
        0
    );
}
value(NODE) ::= INTEGER(I).
{
    NODE = tree_create_node(
        payload_create(N_VALUE, V_INTEGER, I->content),
        0
    );
}
value(NODE) ::= TRUE(T).
{
    NODE = tree_create_node(
        payload_create(N_VALUE, V_BOOLEAN, T->content),
        0
    );
}
value(NODE) ::= FALSE(F).
{
    NODE = tree_create_node(
        payload_create(N_VALUE, V_BOOLEAN, F->content),
        0
    );
}
value(NODE) ::= list(L). { NODE = L; }

/* THIS */

this(NODE) ::= PHRASE(P) DOT THIS.
{
    NODE = tree_create_node(
        payload_create(N_VARIABLE, P->content),
        0
    );
}
this(NODE) ::= THIS.
{
    NODE = tree_create_node(
        payload_create(N_VARIABLE, NULL),
        0
    );
}

/* LIST */

list(NODE) ::= LBRACKET RBRACKET.
{
    NODE = tree_create_node(
        payload_create(N_LIST),
        0
    );
}
list(NODE) ::= LBRACKET expression_sequence(ES) RBRACKET.
{
    NODE = ES;
    struct payload *payload = NODE->payload;
    payload->type = N_LIST;
}

/* VARIABLE */

variable(NODE) ::= PHRASE(P) LBRACE expression(E) RBRACE.
{
    NODE = tree_create_node(
        payload_create(N_VARIABLE, P->content),
        1, E
    );
}
variable(NODE) ::= PHRASE(P) LBRACE RBRACE.
{
    NODE = tree_create_node(
        payload_create(N_VARIABLE, P->content),
        0
    );
}
variable(NODE) ::= PHRASE(P).
{
    NODE = tree_create_node(
        payload_create(N_VARIABLE, P->content),
        0
    );
}
variable(NODE) ::= this(T). { NODE = T; }

/* FUNCTION_CALL */

function_call(NODE) ::= PHRASE(P) LPAREN RPAREN.
{
    NODE = tree_create_node(
        payload_create(N_FUNCTION_CALL, P->content),
        0
    );
}
function_call(NODE) ::= PHRASE(P) LPAREN expression_sequence(ES) RPAREN.
{
    NODE = tree_create_node(
        payload_create(N_FUNCTION_CALL, P->content),
        1, ES
    );
}
