#include <hashmap.h>
#include "result.h"

void result_add(struct result *result, void *data, char *key)
{
    if (result->type == R_LIST) {
        deque_push_last(&(result->data), data);
    } else if (result->type == R_DICT) {
        struct hashmap_entry *entry = malloc(sizeof(struct hashmap_entry));
        entry->key = key;
        entry->value = data;

        deque_push_last(&(result->data), entry);
    }
}
