%{
#include "parser.h"
%}

%option reentrant
%option noyywrap
%option noinput
%option nounput
%option yylineno

%x COMMENT
%x LINE_COMMENT

%%

"select"               return SELECT;
"this"                 return THIS;
"from"                 return FROM;
"where"                return WHERE;
"as"                   return AS;
"list"                 return LIST;
"dict"                 return DICT;

"true"                 return TRUE;
"false"                return FALSE;
"and"                  return AND;
"or"                   return OR;
"xor"                  return XOR;
"not"                  return NOT;
"in"                   return IN;

";"                    return SEMIC;
","                    return COMMA;
"."                    return DOT;
"="                    return ASSIGN;
"*"                    return MUL;
"/"
"%"                    return MOD;
"^"                    return POW;
"{"                    return LBRACE;
"}"                    return RBRACE;
"["                    return LBRACKET;
"]"                    return RBRACKET;
"("                    return LPAREN;
")"                    return RPAREN;

"=="                   return EQ;
"!="                   return NE;
"<="                   return LE;
">="                   return GE;
"<"                    return LT;
">"                    return GT;

\"[^"]*\"              return STRING;
\'[^']*\'              return STRING;
[a-zA-Z_][a-zA-Z0-9_]* return PHRASE;
[0-9]+                 return INTEGER;
[0-9]*\.[0-9]*         return FLOAT;

[ \t\n]+               ;

"/*"                   BEGIN(COMMENT);
<COMMENT>"*/"          BEGIN(INITIAL);
<COMMENT>.             ;

"#"                    BEGIN(LINE_COMMENT);
<LINE_COMMENT>"\n"     BEGIN(INITIAL);
<LINE_COMMENT>.        ;

%%
