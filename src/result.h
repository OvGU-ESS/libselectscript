#ifndef RESULT_H
#define RESULT_H

#include <deque.h>

enum result_type {
    R_NONE,
    R_LIST,
    R_DICT
};

struct result {
    enum result_type type;
    struct deque *data;
};

void result_add(struct result *result, void *data, char *key);

#endif
