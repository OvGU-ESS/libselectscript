#include <stdlib.h>
#include <stdio.h>
#include "cross.h"

void cross_add_list(struct cross **cross, struct deque *list)
{
    if (*cross) {
        (*cross)->count += 1;
        (*cross)->lists = realloc((*cross)->lists,
                                  (*cross)->count * sizeof(struct deque *));
    } else {
        *cross = malloc(sizeof(struct cross));
        (*cross)->count = 1;
        (*cross)->lists = malloc(sizeof(struct deque *));
    }

    (*cross)->lists[(*cross)->count - 1] = list;
}

void cross_free(struct cross **cross)
{
    if (*cross) {
        free(*cross);
        *cross = NULL;
    }
}

struct cross_iterator *cross_iterator_init(struct cross *cross)
{
    struct cross_iterator *cit = malloc(sizeof(struct cross_iterator));
    cit->cross = cross;
    cit->iterators = malloc(cross->count * sizeof(struct deque_iterator));
    cit->values = malloc(cross->count * sizeof(void *));
    cit->values_length = cross->count;

    for (int i = 0; i < cross->count; i++) {
        cit->iterators[i] = deque_iterator_init(&(cit->cross->lists[i]));
        cit->values[i] = deque_iterator_next(cit->iterators[i]);
    }

    return cit;
}

int cross_iterator_next(struct cross_iterator *iterator, void ***values)
{
    int count = iterator->cross->count;

    for (int i = 0; i < count; i++) {
        if (iterator->values[i] == NULL) {
            if (i == count - 1) {
                return 0;
            } else {
                iterator->values[i + 1] =
                    deque_iterator_next(iterator->iterators[i + 1]);
                for (int j = i; j >= 0; j--) {
                    deque_iterator_free(iterator->iterators[j]);
                    iterator->iterators[j] =
                        deque_iterator_init(&(iterator->cross->lists[j]));
                    iterator->values[j] =
                        deque_iterator_next(iterator->iterators[j]);
                }
            }
        }
    }

    for (int i = 0; i < count; i++) {
        (*values)[i] = iterator->values[i];
    }

    // TODO what does this do???
    iterator->values[0] = deque_iterator_next(iterator->iterators[0]);

    return count;
}

void cross_iterator_free(struct cross_iterator *iterator)
{
    if (iterator) {
        free(iterator->iterators);
        free(iterator->values);
        free(iterator);
    }
}
