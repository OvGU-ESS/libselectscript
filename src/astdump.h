#ifndef ASTDUMP_H
#define ASTDUMP_H

#include <tree.h>
#include "payload.h"

void dump_ast(struct node *root, const char *path);

#endif
