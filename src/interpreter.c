#include <hashmap.h>
#include <tree.h>
#include <deque.h>
#include <string.h>
#include <math.h>
#include "interpreter.h"
#include "payload.h"
#include "cross.h"
#include "scope.h"
#include "container.h"
#include "selectscript.h"
#include "library.h"
#include "environment.h"
#include "result.h"
#include "env.h"

/* value operations */

struct container *interpret_value(struct node *value)
{
    struct payload *payload = value->payload;

    struct container *container = malloc(sizeof(struct container));

    // TODO implement external data type

    switch (payload->value.type) {
        case V_STRING:
            container->type = C_STRING;
            container->strng = strdup(payload->value.str);
            break;
        case V_FLOAT:
            container->type = C_FLOAT;
            container->flt = payload->value.flt;
            break;
        case V_INTEGER:
            container->type = C_INTEGER;
            container->ntgr = payload->value.ntgr;
            break;
        case V_BOOLEAN:
            container->type = C_BOOLEAN;
            container->bln = payload->value.bln;
            break;
    }

    return container;
}

/* helper functions */

struct deque *interpret_list(struct node *list)
{
    struct deque *deque = NULL;

    for (int child_idx = 0; child_idx < list->childc; child_idx++) {
        deque_push_last(&deque, interpret_value(list->childv[child_idx]));
    }

    return deque;
}

/* variable operations */

struct container *interpret_variable(struct node *variable)
{
    struct payload *payload = variable->payload;

    /* inside the where tree, no struct env is created -> cast to struct container */
    struct container **ref = (struct container **)(payload->variable.ref);

    return *ref;
}

/* function call */

struct container *interpret_function_call(struct node *function_call)
{
    return NULL;
}

/* arithmetic operations */

struct container *interpret_arithmetic_expression(struct node *expression);

struct container *interpret_arithmetic_add(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_FLOAT && lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_FLOAT:
            result->flt = lhs_container->flt + rhs_container->flt;
            break;
        case C_INTEGER:
            result->ntgr = lhs_container->ntgr + rhs_container->ntgr;
            break;
        case C_EXTERNAL:
            result->ext = library.add(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_sub(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_FLOAT && lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_FLOAT:
            result->flt = lhs_container->flt - rhs_container->flt;
            break;
        case C_INTEGER:
            result->ntgr = lhs_container->ntgr - rhs_container->ntgr;
            break;
        case C_EXTERNAL:
            result->ext = library.sub(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_mul(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_FLOAT && lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_FLOAT:
            result->flt = lhs_container->flt * rhs_container->flt;
            break;
        case C_INTEGER:
            result->ntgr = lhs_container->ntgr * rhs_container->ntgr;
            break;
        case C_EXTERNAL:
            result->ext = library.mul(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_div(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_FLOAT && lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_FLOAT:
            result->flt = lhs_container->flt / rhs_container->flt;
            break;
        case C_INTEGER:
            result->ntgr = lhs_container->ntgr / rhs_container->ntgr;
            break;
        case C_EXTERNAL:
            result->ext = library.div(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_mod(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_INTEGER:
            result->ntgr = lhs_container->ntgr % rhs_container->ntgr;
            break;
        case C_EXTERNAL:
            result->ext = library.mod(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_pow(struct node *lhs,
        struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    if (lhs_container->type != rhs_container->type) {
        return NULL;
    }

    if (lhs_container->type != C_FLOAT && lhs_container->type != C_INTEGER) {
        return NULL;
    }

    struct container *result = malloc(sizeof(struct container));

    switch (lhs_container->type) {
        case C_FLOAT:
            result->flt = powf(lhs_container->flt, rhs_container->flt);
            break;
        case C_INTEGER:
            result->ntgr = (int)pow(lhs_container->ntgr, rhs_container->ntgr);
            break;
        case C_EXTERNAL:
            result->ext = library.pow(lhs_container->ext, rhs_container->ext);
            break;
        default:
            return NULL;
    }

    result->type = lhs_container->type;

    return result;
}

struct container *interpret_arithmetic_negation(struct node *expression)
{
    // TODO
    return NULL;
}

struct container *interpret_arithmetic_expression(struct node *expression)
{
    struct payload *payload = expression->payload;

    switch (payload->type) {
        case N_ARITHMETIC_ADD_SUB:
            if (payload->arithmetic_add_sub.type == AAS_ADD) {
                return interpret_arithmetic_add(expression->childv[0],
                                                expression->childv[1]);
            } else {
                return interpret_arithmetic_sub(expression->childv[0],
                                                expression->childv[1]);
            }
            break;
        case N_ARITHMETIC_MUL_DIV_MOD:
            if (payload->arithmetic_mul_div_mod.type == AMDM_MUL) {
                return interpret_arithmetic_mul(expression->childv[0],
                                                expression->childv[1]);
            } else if (payload->arithmetic_mul_div_mod.type == AMDM_DIV) {
                return interpret_arithmetic_div(expression->childv[0],
                                                expression->childv[1]);
            } else {
                return interpret_arithmetic_mod(expression->childv[0],
                                                expression->childv[1]);
            }
            break;
        case N_ARITHMETIC_POW:
            return interpret_arithmetic_pow(expression->childv[0],
                                            expression->childv[1]);
            break;
        case N_ARITHMETIC_NEGATION:
            // TODO implement
            return NULL;
            break;
        case N_VALUE:
            return interpret_value(expression);
            break;
        case N_VARIABLE:
            return interpret_variable(expression);
            break;
        case N_FUNCTION_CALL:
            return interpret_function_call(expression);
            break;
        default:
            return NULL;
    }
}

/* comparison operations */

int interpret_comparison_eq(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 0;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                ret = strcmp(lhs_container->strng, rhs_container->strng) == 0 ? 1 : 0;
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt == rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr == rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                ret = lhs_container->bln == rhs_container->bln;
                break;
            case C_EXTERNAL:
                ret = library.eq(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_ne(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 1;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                ret = strcmp(lhs_container->strng, rhs_container->strng);
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt != rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr != rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                ret = lhs_container->bln != rhs_container->bln;
                break;
            case C_EXTERNAL:
                ret = library.ne(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_ge(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 0;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                // this operation is not defined for strings
                ret = 0;
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt >= rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr >= rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                // this operation is not defined for booleans
                ret = 0;
                break;
            case C_EXTERNAL:
                ret = library.ge(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_gt(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 0;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                // this operation is not defined for strings
                ret = 0;
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt > rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr > rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                // this operation is not defined for booleans
                ret = 0;
                break;
            case C_EXTERNAL:
                ret = library.gt(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_le(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 0;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                // this operation is not defined for strings
                ret = 0;
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt <= rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr <= rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                // this operation is not defined for booleans
                ret = 0;
                break;
            case C_EXTERNAL:
                ret = library.le(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_lt(struct node *lhs, struct node *rhs)
{
    struct container *lhs_container = interpret_arithmetic_expression(lhs);
    struct container *rhs_container = interpret_arithmetic_expression(rhs);

    int ret = 0;

    if (lhs_container->type == rhs_container->type) {
        switch (lhs_container->type) {
            case C_STRING:
                // this operation is not defined for strings
                ret = 0;
                break;
            case C_FLOAT:
                // TODO think about float inaccuracy
                ret = lhs_container->flt < rhs_container->flt;
                break;
            case C_INTEGER:
                ret = lhs_container->ntgr < rhs_container->ntgr;
                break;
            case C_BOOLEAN:
                // this operation is not defined for booleans
                ret = 0;
                break;
            case C_EXTERNAL:
                ret = library.lt(lhs_container->ext, rhs_container->ext);
                break;
        }
    }

    return ret;
}

int interpret_comparison_expression(struct node *comparison_expression)
{
    struct payload *comparison_payload = comparison_expression->payload;

    switch (comparison_payload->type) {
        case N_COMPARISON_IN:
            return 0;
        case N_COMPARISON_EQ:
            return interpret_comparison_eq(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        case N_COMPARISON_NE:
            return interpret_comparison_ne(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        case N_COMPARISON_GE:
            return interpret_comparison_ge(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        case N_COMPARISON_GT:
            return interpret_comparison_gt(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        case N_COMPARISON_LE:
            return interpret_comparison_le(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        case N_COMPARISON_LT:
            return interpret_comparison_lt(
                       comparison_expression->childv[0],
                       comparison_expression->childv[1]
                   );
            break;
        default:
            return 0; //interpret_arithmetic_expression(comparison_expression);
    }
}

/* logic operations */

int interpret_logic_expression(struct node *logic_expression)
{
    struct payload *logic_payload = logic_expression->payload;
    int lhs = 0;
    int rhs = 0;

    switch (logic_payload->type) {
        case N_LOGIC_OR:
            lhs = interpret_logic_expression(logic_expression->childv[0]);
            rhs = interpret_logic_expression(logic_expression->childv[1]);
            return lhs || rhs;
        case N_LOGIC_XOR:
            lhs = interpret_logic_expression(logic_expression->childv[0]);
            rhs = interpret_logic_expression(logic_expression->childv[1]);
            return (lhs && !rhs) || (!lhs && rhs);
        case N_LOGIC_AND:
            lhs = interpret_logic_expression(logic_expression->childv[0]);
            rhs = interpret_logic_expression(logic_expression->childv[1]);
            return lhs && rhs;
        case N_LOGIC_NOT:
            lhs = interpret_logic_expression(logic_expression->childv[0]);
            return !lhs;
        default:
            return interpret_comparison_expression(logic_expression);
    }
}

/* where operation */

int interpret_where(struct node *where)
{
    return interpret_logic_expression(where->childv[0]);
}

/* selection operation */

struct deque *interpret_selection(struct node *selection)
{
    struct payload *selection_payload = selection->payload;

    struct node *where = NULL;
    struct payload *where_payload = NULL;
    if (selection_payload->selection.flags.where) {
        /* process where clause */
        where = selection->childv[2];
        where_payload = where->payload;
    } else {
        /* skip processing non existent where clause and return */
        return NULL; // TODO fix this
    }

    struct cross *cross = NULL;

    struct node *from = selection->childv[1];
    struct deque **lists = malloc(from->childc * sizeof(struct deque *));

    char **lookup = malloc(from->childc * sizeof(char *));

    /* loop over all 'from' elements */
    for (int child_idx = 0; child_idx < from->childc; child_idx++) {
        /* get assign expression */
        struct node *assign_expr = from->childv[child_idx];
        struct payload *assign_expr_payload = assign_expr->payload;

        /*
         * lookup table for from element assignment phrases by position
         *
         * associate phrase of assign expression with index of assign
         * expression
         */
        lookup[child_idx] = strdup(assign_expr_payload->assign_expression.phrase);

        /* get variable from assign expression */
        struct node *variable = assign_expr->childv[0];
        struct payload *variable_payload = variable->payload;

        /* get referenced element */
        struct env *ref = variable_payload->variable.ref;

        switch (ref->type) {
            case ENV_NODE:
                ;
                struct node *ref_elem = ref->node->childv[0];
                struct payload *ref_elem_payload = ref_elem->payload;

                /* add element to deque */
                if (ref_elem_payload->type == N_LIST) {
                    /* convert list to deque */
                    lists[child_idx] = interpret_list(ref_elem);
                } else {
                    /* push element to deque */
                    deque_push_last(
                        &(lists[child_idx]),
                        interpret_value(ref_elem)
                    );
                }

                break;
            case ENV_VALUE:
                deque_push_last(&(lists[child_idx]), ref->value);
                break;
            case ENV_LIST:
                lists[child_idx] = ref->list;
                break;
        }

        /* add deque to cross product */
        cross_add_list(&cross, lists[child_idx]);
    }

    /* create cross product iterator */
    struct cross_iterator *cit = cross_iterator_init(cross);

    /* create array in the size of the number of cross lists */
    int values_length = cit->values_length;
    struct container **value = malloc(values_length * sizeof(struct container *));

    /* put value pointers into where scope */
    for (int v = 0; v < values_length; v++) {
        hashmap_put(&(where_payload->where.scope), lookup[v], value + v);
    }

    /* link references for all nodes inside the where tree to value pointers */
    link_references(where);

    /* create result list */
    struct deque *result_list = NULL;

    /*
     * iterate over all elements of the cross product
     *
     * with every iteration, the elements in the values array are swaped with
     * values for the next cross product element. Finally the where tree is
     * evaluated with these new values.
     */
    while (cross_iterator_next(cit, (void ** *)(&value)) != 0) {
        /* calculate where clause */
        if (interpret_where(where)) {
            /* create result list */
            struct result *result = malloc(sizeof(struct result));
            result->data = NULL;
            if (selection_payload->selection.result_type == R_LIST ||
                    selection_payload->selection.result_type == R_NONE) {
                result->type = R_LIST;
            } else if (selection_payload->selection.result_type == R_DICT) {
                result->type = R_DICT;
            }

            /* insert results into result list */
            for (int v_idx = 0; v_idx < values_length; v_idx++) {
                result_add(result, value[v_idx], lookup[v_idx]);
            }

            /* append local result list to overall results */
            deque_push_last(&result_list, result);
        }
    }

    free(value);

    return result_list;
}

struct deque *interpret(struct parser_state *parser_state)
{
    struct node *state_seq = parser_state->ast;

    for (int i = 0; i < state_seq->childc; i++) {
        struct node *statement = state_seq->childv[i];
        struct payload *state_payload = statement->payload;

        if (state_payload->type == N_SELECTION) {
            return interpret_selection(statement);
        }
    }

    return NULL;
}
