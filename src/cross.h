#ifndef CROSS_H
#define CROSS_H

#include <deque.h>

struct cross {
    int count;
    struct deque **lists;
};

struct cross_iterator {
    struct cross *cross;
    struct deque_iterator **iterators;
    int values_length;
    void **values;
};

void cross_add_list(struct cross **cross, struct deque *list);

void cross_free(struct cross **cross);

struct cross_iterator *cross_iterator_init(struct cross *cross);

int cross_iterator_next(struct cross_iterator *iterator, void ***values);

void cross_iterator_free(struct cross_iterator *iterator);

#endif
