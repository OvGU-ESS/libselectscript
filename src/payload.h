#ifndef PAYLOAD_H
#define PAYLOAD_H

#include "container.h"
#include "result.h"
#include "env.h"

enum type {
    N_TRANSLATION_UNIT,

    N_SELECTION,
    N_SELECT,
    N_SELECTOR,
    N_WHERE,

    N_ASSIGNMENT,
    N_EXPRESSION_SEQUENCE,
    N_ASSIGN_EXPRESSION,
    N_VALUE,
    N_LIST,
    N_VARIABLE,
    N_FUNCTION_CALL,
    N_IF,

    N_LOGIC_OR,
    N_LOGIC_XOR,
    N_LOGIC_AND,
    N_LOGIC_NOT,

    N_COMPARISON_IN,
    N_COMPARISON_EQ,
    N_COMPARISON_NE,
    N_COMPARISON_GE,
    N_COMPARISON_GT,
    N_COMPARISON_LE,
    N_COMPARISON_LT,

    N_ARITHMETIC_ADD_SUB,
    N_ARITHMETIC_MUL_DIV_MOD,
    N_ARITHMETIC_POW,
    N_ARITHMETIC_NEGATION
};

enum value_type {
    V_STRING,
    V_FLOAT,
    V_INTEGER,
    V_BOOLEAN
};

enum arithmetic_add_sub_type {
    AAS_ADD,
    AAS_SUB
};

enum arithmetic_mul_div_mod_type {
    AMDM_MUL,
    AMDM_DIV,
    AMDM_MOD
};

struct selection_flags {
    int where : 1;
    int as : 1;
};

struct payload {
    enum type type;

    union {
        struct {
            struct hashmap *scope;
        } translation_unit;
        struct {
            struct selection_flags flags;
            struct hashmap *scope;
            enum result_type result_type;
        } selection;
        struct {
            enum value_type type;
            union {
                char *str;
                float flt;
                int ntgr;
                int bln;
            };
        } value;
        struct {
            char *phrase;
            struct env *ref;
        } variable;
        struct {
            char *phrase;
        } assignment;
        struct {
            char *phrase;
        } function_call;
        struct {
            char *phrase;
        } selector;
        struct {
            char *phrase;
        } assign_expression;
        struct {
            struct hashmap *scope;
        } where;
        struct {
            enum arithmetic_add_sub_type type;
        } arithmetic_add_sub;
        struct {
            enum arithmetic_mul_div_mod_type type;
        } arithmetic_mul_div_mod;
    };
};

struct payload *payload_create(enum type, ...);

void payload_free(void *payload);

#endif
