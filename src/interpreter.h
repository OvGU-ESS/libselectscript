#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <deque.h>
#include "parser_state.h"

struct deque *interpret(struct parser_state *parser_state);

#endif
