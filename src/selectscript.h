#ifndef SELECTSCRIPT_H
#define SELECTSCRIPT_H

#include <hashmap.h>
#include <deque.h>
#include "parser_state.h"
#include "interpreter.h"
#include "env.h"

enum function_id {
    FID_ADD,
    FID_SUB,
    FID_MUL,
    FID_DIV,
    FID_MOD,
    FID_POW,
    FID_EQ,
    FID_NE,
    FID_GE,
    FID_GT,
    FID_LE,
    FID_LT
};

struct library {
    void *(*add)(void *, void *);
    void *(*sub)(void *, void *);
    void *(*mul)(void *, void *);
    void *(*div)(void *, void *);
    void *(*mod)(void *, void *);
    void *(*pow)(void *, void *);
    int (*eq)(void *, void *);
    int (*ne)(void *, void *);
    int (*ge)(void *, void *);
    int (*gt)(void *, void *);
    int (*le)(void *, void *);
    int (*lt)(void *, void *);
};

struct ss_state {
    struct parser_state parser_state;
    struct deque *result;
};

void add_library_function(enum function_id fid, ...);

void add_environment_element(enum env_type type, const char *name,
                             void *element);

struct ss_state *ss_new(const char *script);

void ss_evaluate(struct ss_state *state);

void ss_free(struct ss_state *state);

#endif
