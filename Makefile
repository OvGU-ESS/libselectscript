LIB = libselectscript.a

CFLAGS = -std=gnu11 -Wall -g -Ilibcollect -fPIC
LDFLAGS = -Llibcollect
LDLIBS = -lcollect -lm
YACC = lemon/lemon
LEX = flex
LFLAGS = --header-file=src/lexer.h

SOURCES = \
src/lexer.l \
src/parser.y \
src/selectscript.c \
src/payload.c \
src/astdump.c \
src/interpreter.c \
src/scope.c \
src/cross.c \
src/stop_watch.c \
src/result.c

COBJECTS = $(patsubst %.c, %.o, $(SOURCES))
LOBJECTS = $(patsubst %.l, %.o, $(COBJECTS))
OBJECTS = $(patsubst %.y, %.o, $(LOBJECTS))

.PHONY: all test valgrind libcollect lemon style examples clean

all: $(LIB)

$(LIB): $(OBJECTS) libcollect
	ar -rcs $@ $(OBJECTS)

src/lexer.c: src/lexer.l src/parser.c
	$(LEX) $(LFLAGS) -o $@ $<

src/parser.c: src/parser.y lemon
	$(YACC) $<

SPECK_CFLAGS = -Ilibcollect -Isrc
SPECK_LDFLAGS = -Llibcollect -L.
SPECK_LIBS = \
-lselectscript \
-lcollect \
-lm
-include speck/speck.mk

test: $(SPECK) $(LIB) $(SUITES)
	@$(SPECK)

valgrind: $(SPECK) $(LIB) $(SUITES)
	@valgrind --leak-check=full --error-exitcode=1 $(SPECK)

libcollect:
	@make -C libcollect

lemon:
	@make -C lemon

getexternals:
	git submodule init
	git submodule update

style:
	astyle -A3s4SpHk3jnr "src/*.c" "src/*.h" "spec/*.c" "examples/*.c"

examples: $(LIB)
	@make -C examples

clean:
	rm -f $(LIB) $(OBJECTS) src/lexer.c src/lexer.h src/parser.c src/parser.h src/parser.out
	rm -f spec/*.so
	@make -C examples clean
