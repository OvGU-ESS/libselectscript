#include <speck.h>
#include <tree.h>
#include <string.h>
#include "../src/payload.h"
#include "../src/container.h"

struct container *interpret_value(struct node *value);
struct container *interpret_arithmetic_add(struct node *lhs,
        struct node *rhs);
struct container *interpret_arithmetic_sub(struct node *lhs,
        struct node *rhs);
struct container *interpret_arithmetic_mul(struct node *lhs,
        struct node *rhs);
struct container *interpret_arithmetic_div(struct node *lhs,
        struct node *rhs);
struct container *interpret_arithmetic_mod(struct node *lhs,
        struct node *rhs);
struct container *interpret_arithmetic_pow(struct node *lhs,
        struct node *rhs);
int interpret_comparison_eq(struct node *lhs, struct node *rhs);
int interpret_comparison_ne(struct node *lhs, struct node *rhs);
int interpret_comparison_ge(struct node *lhs, struct node *rhs);
int interpret_comparison_gt(struct node *lhs, struct node *rhs);
int interpret_comparison_le(struct node *lhs, struct node *rhs);
int interpret_comparison_lt(struct node *lhs, struct node *rhs);

/* value */

void spec_value_integer(void)
{
    struct node *value = tree_create_node(
                             payload_create(N_VALUE, V_INTEGER, "42"),
                             0
                         );

    struct container *container = interpret_value(value);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 42);

    free(container);
    tree_free(&value, payload_free);
}

void spec_value_float(void)
{
    struct node *value = tree_create_node(
                             payload_create(N_VALUE, V_FLOAT, "42.23"),
                             0
                         );

    struct container *container = interpret_value(value);

    sp_assert(container->type == C_FLOAT);

    free(container);
    tree_free(&value, payload_free);
}

void spec_value_string(void)
{
    struct node *value = tree_create_node(
                             payload_create(N_VALUE, V_STRING, "TEST"),
                             0
                         );

    struct container *container = interpret_value(value);

    sp_assert(container->type == C_STRING);
    sp_assert(strcmp(container->strng, "TEST") == 0);

    free(container->strng);
    free(container);
    tree_free(&value, payload_free);
}

void spec_value_boolean(void)
{
    struct node *value = tree_create_node(
                             payload_create(N_VALUE, V_BOOLEAN, "true"),
                             0
                         );

    struct container *container = interpret_value(value);

    sp_assert(container->type == C_BOOLEAN);
    sp_assert(container->bln);

    free(container);
    tree_free(&value, payload_free);
}

/* arithmetic */

void spec_arithmetic_add(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "20"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "10"),
                           0
                       );

    struct container *container = interpret_arithmetic_add(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 30);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_arithmetic_sub(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "20"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "5"),
                           0
                       );

    struct container *container = interpret_arithmetic_sub(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 15);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_arithmetic_mul(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "20"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "5"),
                           0
                       );

    struct container *container = interpret_arithmetic_mul(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 100);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_arithmetic_div(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "20"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "5"),
                           0
                       );

    struct container *container = interpret_arithmetic_div(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 4);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_arithmetic_mod(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "2"),
                           0
                       );

    struct container *container = interpret_arithmetic_mod(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 1);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_arithmetic_pow(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "2"),
                           0
                       );

    struct container *container = interpret_arithmetic_pow(lhs, rhs);

    sp_assert(container->type == C_INTEGER);
    sp_assert(container->ntgr == 81);

    free(container);
    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

/* comparison */

void spec_comparison_eq(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );

    int result = interpret_comparison_eq(lhs, rhs);

    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_comparison_ne(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "7"),
                           0
                       );

    int result = interpret_comparison_ne(lhs, rhs);

    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
}

void spec_comparison_ge(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *extra = tree_create_node(
                             payload_create(N_VALUE, V_INTEGER, "7"),
                             0
                         );

    int result = interpret_comparison_ge(lhs, rhs);
    sp_assert(result);

    result = interpret_comparison_ge(lhs, extra);
    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
    tree_free(&extra, payload_free);
}

void spec_comparison_gt(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *extra = tree_create_node(
                             payload_create(N_VALUE, V_INTEGER, "7"),
                             0
                         );

    int result = interpret_comparison_gt(lhs, rhs);
    sp_assert(result == 0);

    result = interpret_comparison_gt(lhs, extra);
    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
    tree_free(&extra, payload_free);
}

void spec_comparison_le(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *extra = tree_create_node(
                             payload_create(N_VALUE, V_INTEGER, "7"),
                             0
                         );

    int result = interpret_comparison_le(lhs, rhs);
    sp_assert(result);

    result = interpret_comparison_le(extra, rhs);
    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
    tree_free(&extra, payload_free);
}

void spec_comparison_lt(void)
{
    struct node *lhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *rhs = tree_create_node(
                           payload_create(N_VALUE, V_INTEGER, "9"),
                           0
                       );
    struct node *extra = tree_create_node(
                             payload_create(N_VALUE, V_INTEGER, "7"),
                             0
                         );

    int result = interpret_comparison_lt(lhs, rhs);
    sp_assert(result == 0);

    result = interpret_comparison_lt(extra, rhs);
    sp_assert(result);

    tree_free(&lhs, payload_free);
    tree_free(&rhs, payload_free);
    tree_free(&extra, payload_free);
}
