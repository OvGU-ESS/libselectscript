#include <speck.h>
#include "../src/lexer.h"
#include "../src/parser.h"

void spec_phrase(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("a ab aZ b9 h_O9", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}

void spec_comment(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("abcd /* efgh */ ijkl", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}

void spec_line_comment(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("abcd # efgh ijkl", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == PHRASE);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}

void spec_string(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("\"foo\" 'bar'", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == STRING);
    lex_code = yylex(scanner);
    sp_assert(lex_code == STRING);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}

void spec_integer(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("03483 1 0", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == INTEGER);
    lex_code = yylex(scanner);
    sp_assert(lex_code == INTEGER);
    lex_code = yylex(scanner);
    sp_assert(lex_code == INTEGER);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}

void spec_float(void)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string("1. .5 13.37", scanner);

    int lex_code;
    lex_code = yylex(scanner);
    sp_assert(lex_code == FLOAT);
    lex_code = yylex(scanner);
    sp_assert(lex_code == FLOAT);
    lex_code = yylex(scanner);
    sp_assert(lex_code == FLOAT);
    lex_code = yylex(scanner);
    sp_assert(lex_code == 0);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);
}
