#include <speck.h>
#include <deque.h>
#include <stdio.h>
#include "../src/cross.h"

void spec_cross(void)
{
    int values[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    struct deque *list_a = NULL;
    struct deque *list_b = NULL;
    struct deque *list_c = NULL;

    for (int i = 0; i < 7; i++) {
        deque_push_last(&list_a, values + i);
    }

    for (int i = 0; i < 4; i++) {
        deque_push_last(&list_b, values + i);
    }

    for (int i = 0; i < 2; i++) {
        deque_push_last(&list_c, values + i);
    }

    struct cross *cross = NULL;
    cross_add_list(&cross, list_a);
    cross_add_list(&cross, list_b);
    cross_add_list(&cross, list_c);

    struct cross_iterator *cit = cross_iterator_init(cross);

    void **value = malloc(cit->values_length * sizeof(void *));
    int counter = 0;
    while (cross_iterator_next(cit, &value) != 0) {
        // for (int i = 0; i < cit->values_length; i++) {
        //     printf("%d ", *((int **)value)[i]);
        // }
        // puts("");

        counter++;
    }

    free(value);

    cross_iterator_free(cit);

    cross_free(&cross);

    deque_free(&list_a, NULL);
    deque_free(&list_b, NULL);
    deque_free(&list_c, NULL);

    sp_assert_equal_i(counter, 56);
}
