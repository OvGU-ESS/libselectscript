#include <speck.h>
#include "../src/parser.h"
#include "../src/parser_signatures.h"
#include "../src/lexer.h"
#include "../src/token.h"

/* helper */

enum states integrate(const char *code)
{
    yyscan_t scanner;
    yylex_init(&scanner);
    YY_BUFFER_STATE bufferState = yy_scan_string(code, scanner);

    struct parser_state parser_state;
    void *parser = ParseAlloc(malloc);

    int lex_code = 0;
    struct token *token = NULL;
    while ((lex_code = yylex(scanner))) {
        if (lex_code == -1) {
            break;
        }

        token = malloc(sizeof(struct token));
        token->content = strdup(yyget_text(scanner));
        token->lineno = yyget_lineno(scanner);
        Parse(parser, lex_code, token, &parser_state);
    }

    Parse(parser, 0, NULL, &parser_state);
    ParseFree(parser, free);

    yy_delete_buffer(bufferState, scanner);
    yylex_destroy(scanner);

    return parser_state.state;
}

/* tests */

void spec_simple_select(void)
{
    const char *code = "select * from 42;";
    enum states state = integrate(code);
    sp_assert_equal_i(state, OK);
}
