#include <speck.h>
#include "../src/parser.h"
#include "../src/parser_signatures.h"
#include "../src/token.h"
#include <string.h>

struct token *new_token(const char *text)
{
    struct token *token = malloc(sizeof(struct token));
    if (text) {
        token->content = strdup(text);
    } else {
        token->content = NULL;
    }
    token->lineno = 42;

    return token;
}

void spec_select(void)
{
    struct parser_state parser_state;
    void *parser = ParseAlloc(malloc);

    Parse(parser, SELECT, new_token("select"), &parser_state);
    Parse(parser, MUL, new_token("*"), &parser_state);

    Parse(parser, FROM, new_token("from"), &parser_state);
    Parse(parser, STRING, new_token("'string'"), &parser_state);

    Parse(parser, WHERE, new_token("where"), &parser_state);
    Parse(parser, PHRASE, new_token("a"), &parser_state);
    Parse(parser, EQ, new_token("=="), &parser_state);
    Parse(parser, INTEGER, new_token("1"), &parser_state);

    Parse(parser, SEMIC, new_token(";"), &parser_state);

    Parse(parser, 0, NULL, &parser_state);
    ParseFree(parser, free);

    sp_assert_equal_i(parser_state.state, OK);
}

void spec_assignment(void)
{
    struct parser_state parser_state;
    void *parser = ParseAlloc(malloc);

    Parse(parser, PHRASE, new_token("phrase"), &parser_state);
    Parse(parser, LBRACE, new_token("{"), &parser_state);
    Parse(parser, PHRASE, new_token("phrase"), &parser_state);
    Parse(parser, RBRACE, new_token("}"), &parser_state);
    Parse(parser, ASSIGN, new_token("="), &parser_state);
    Parse(parser, INTEGER, new_token("5"), &parser_state);
    Parse(parser, SEMIC, new_token(";"), &parser_state);

    Parse(parser, 0, NULL, &parser_state);
    ParseFree(parser, free);

    sp_assert_equal_i(parser_state.state, OK);
}

void spec_value(void)
{
    struct parser_state parser_state;
    void *parser = ParseAlloc(malloc);

    Parse(parser, SELECT, new_token("select"), &parser_state);
    Parse(parser, MUL, new_token("*"), &parser_state);

    Parse(parser, FROM, new_token("from"), &parser_state);
    Parse(parser, LBRACKET, new_token("["), &parser_state);
    Parse(parser, INTEGER, new_token("1"), &parser_state);
    Parse(parser, COMMA, new_token(","), &parser_state);
    Parse(parser, LPAREN, new_token("("), &parser_state);
    Parse(parser, PHRASE, new_token("foo"), &parser_state);
    Parse(parser, RPAREN, new_token(")"), &parser_state);
    Parse(parser, RBRACKET, new_token("]"), &parser_state);
    Parse(parser, SEMIC, new_token(";"), &parser_state);

    Parse(parser, 0, NULL, &parser_state);
    ParseFree(parser, free);

    sp_assert_equal_i(parser_state.state, OK);
}
